//onde fa�o as acoes

package br.ce.wcaquino.pages;

import org.openqa.selenium.By;

import br.ce.wcaquino.core.BasePage;
import br.ce.wcaquino.core.DriverFactory;

public class LoginPage extends BasePage {
	
	public void acessarTelaInicial(){
		DriverFactory.getDriver().get("https://seubarriga.wcaquino.me");

	}
	
	public void  setEmail(String email) {
		escrever("email", email);
	}
	
	public void setSenha(String senha) {
		escrever("senha", senha);
	}
	
	public void setButtonEntra() {
		clicarBotao(By.xpath("//button[@class='btn btn-primary']"));
	}
	
	public void logar(String email, String senha) {
//		acessarTelaInicial();
		setEmail(email);
		setSenha(senha);
		setButtonEntra();
	}
	
}
