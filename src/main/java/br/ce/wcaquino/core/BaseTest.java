//onde preencho as acoes

package br.ce.wcaquino.core;
import static br.ce.wcaquino.core.DriverFactory.KillDriver;
import static br.ce.wcaquino.core.DriverFactory.getDriver;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import br.ce.wcaquino.pages.LoginPage;

public class BaseTest {
	private LoginPage page = new LoginPage();
	
	
	@Rule
	public TestName testname = new TestName();
	
	@Before
	public void inicializar() {
		page.acessarTelaInicial();
		
		page.setEmail("daniel.luis.siqueira@gmail.com");
		page.setSenha("19121995");
		page.setButtonEntra();
//		page.logar("daniel.luis.siqueira@gmail.com", "19121995");
		
	}
	
	@After
	public void finaliza() throws IOException {
		TakesScreenshot ss = (TakesScreenshot) getDriver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo,new File("target"+ File.separator +"screenshot" + 
				 File.separator + testname.getMethodName() + ".jpg"));
		
		if(Propriedades.FECHAR_BROWSER) {
			KillDriver();
		} 
	}
}
