 package br.ce.wcaquino.tests;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.ce.wcaquino.core.BaseTest;
import br.ce.wcaquino.pages.ContasPage;
import br.ce.wcaquino.pages.MenuPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContaTest extends BaseTest {
	
	MenuPage menuPage = new MenuPage();
	ContasPage contasPage = new ContasPage();
	
	@Test
	public void test1_InserirConta() {
		menuPage.acessarTelaInserirConta();
		
		contasPage.setNome("Bradesco Teste");
		contasPage.salvar();
		
		Assert.assertEquals("Conta adicionada com sucesso!", contasPage.obterMensagemSucesso());
		
	}
	
	@Test 
	public void test2_AlterarConta() {
		menuPage.acessarTelaParaAlterarConta();
		
		contasPage.clicarAlterarConta("Bradesco Teste");
		contasPage.setNome("Itau Teste alterado");
		contasPage.salvar();
		Assert.assertEquals("Conta alterada com sucesso!", contasPage.obterMensagemSucesso());
		
	}
	
	@Test 
	public void test3_InsirirContaComMesmoNome() {
		menuPage.acessarTelaInserirConta();
		
		contasPage.setNome("Itau Teste alterado");
		contasPage.salvar();
		Assert.assertEquals("J� existe uma conta com esse nome!", contasPage.obterMensagemErro());
	}

	
}
